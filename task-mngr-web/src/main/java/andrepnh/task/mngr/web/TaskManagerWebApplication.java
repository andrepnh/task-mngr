package andrepnh.task.mngr.web;

import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class TaskManagerWebApplication extends Application<TaskManagerWebConfiguration> {
    public static void main(String[] args) throws Exception {
        new TaskManagerWebApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<TaskManagerWebConfiguration> bootstrap) {
        bootstrap.addBundle(new AssetsBundle("/assets/", "/"));
    }
    
    @Override
    public void run(TaskManagerWebConfiguration cfg, Environment env) throws Exception {
        env.jersey().disable();
    }

    @Override
    public String getName() {
        return "task-mnrg-web";
    }

}

