package andrepnh.task.mngr.web;

import io.dropwizard.Configuration;
import org.hibernate.validator.constraints.NotEmpty;

public class TaskManagerWebConfiguration extends Configuration {
    @NotEmpty
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    
}
