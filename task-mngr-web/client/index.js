import React from 'react';
import { render } from 'react-dom';
import Login from './components/Login.jsx';
import Tasks from './components/Tasks.jsx';
import { Router, Route, browserHistory, IndexRoute } from 'react-router';

render(
  <Router history={browserHistory}>
    <Route path="/index" component={Login}/>
    <Route path="/index.html" component={Login}/>
	<Route path="/tasks" component={Tasks}/>
  </Router>,
  document.getElementById('root')
);