import React from 'react';
import Bootstrap from 'bootstrap/dist/css/bootstrap.min.css';
import { Modal, Well, ListGroup, ListGroupItem, Panel, PageHeader, Button, Grid, Row, Col } from 'react-bootstrap';
import { parse, format } from 'date-fns';
import FieldGroup from './FieldGroup.jsx';

localStorage.setItem('username', 'joao');

class NewTaskModal extends React.Component {
  constructor(props) {
	super(props);
	this.state = { showModal: false, description: '' };
	this.open = this.open.bind(this);
	this.close = this.close.bind(this);
	this.save = this.save.bind(this);
	this.descriptionChange = this.descriptionChange.bind(this);
  }
  
  open() {
	this.setState({showModal: true});
  }
  
  close() {
	this.setState({showModal: false});
  }
  
  save() {
	let cfg = {
	  mode: 'no-cors',
	  method: 'POST',
	  headers: new Headers({'Content-Type': 'application/json', 'Accept': 'application/json'}),
	  body: JSON.stringify({description: 'teste'})
	};
	fetch(`http://localhost:9090/users/${localStorage.getItem('username')}/tasks`, cfg)
	  .then(r => !r.ok ? console.error('Falha ao salvar tarefa: ' + r.status) : this.close());
	this.props.onNewTask();
  }
  
  descriptionChange(e) {
	this.setState({descrition: e.target.value});
  }
  
  render() {
	return (<div>
	  <Button bsStyle="primary" onClick={this.open}>Nova tarefa</Button>
	  <Modal show={this.state.showModal} onHide={this.close}>
	    <Modal.Header closeButton>
          <Modal.Title>Nova tarefa</Modal.Title>
        </Modal.Header>
        <Modal.Body>
		  <FieldGroup id="description" componentClass="textarea" value={this.state.description} handleChange={this.descriptionChange} 
		    label="Descrição" placeholder="Digite a descrição da tarefa a ser criada"/>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.close}>Cancelar</Button>
		  <Button bsStyle="primary" onClick={this.save}>Criar nova tarefa</Button>
        </Modal.Footer>
	  </Modal>
	</div>);
  }
}

class TaskCard extends React.Component {
  render() {
	let { code, description, status, creationDate, user } = this.props;
	creationDate = format(parse(creationDate), 'DD/MM/YYYY');
    return (
	  <Well>
	    <Row>
		  <Col md={10}>{description}</Col>
	      <Col md={2}>{code}</Col>
		</Row>
		<Row>
		  <Col md={4}>{status === 'TAKEN' && <p>Atribuída para {user}</p>}</Col>
		</Row>
		<Row>
		  <Col md={4}>Criada em {creationDate}</Col>
		</Row>
	  </Well>);
  }	
}

class TaskControl extends React.Component {
  constructor(props) {
    super(props);
	this.state = {tasks: []};
	this.updateTaskList = this.updateTaskList.bind(this);
  }
  
  componentWillMount() {
	this.updateTaskList();
  }
  
  updateTaskList() {
	fetch('http://localhost:9090/tasks')
	  .then(r => r.ok ? r.json() : console.error('Falha ao buscar tarefas: ' + r.status))
	  .then(tasks => this.setState({tasks: tasks}));
  }
	
  render() {
	let items = this.state.tasks
	  .map(({ code, description, status, creationDate, user }) => {
		return (<ListGroupItem key={code}><TaskCard code={code} description={description} status={status} creationDate={creationDate} user={user}/></ListGroupItem>)
	  });
	
    return (
	  <div>
        <Row>
         <Col md={8}>
	       <h3>Tarefas disponíveis</h3>
	     </Col>
	     <Col md={4}>
		   <NewTaskModal onNewTask={this.updateTaskList}/>
	     </Col>
        </Row>
	    <Row>
          <ListGroup>
		    {items}
	      </ListGroup>
        </Row>
	  </div>);
  }
}

class UserInfo extends React.Component {
  render() {
    return (
     <div style={{textAlign: 'right'}}>
       Olá, {this.props.displayName}
     </div>);
  }
}

class Header extends React.Component {
  render() {
    return (
	  <Row>
        <Col md={12}>
		  <PageHeader><small><UserInfo displayName={this.props.displayName}/></small></PageHeader>
		</Col>
	  </Row>);
  }
}

export default class Tasks extends React.Component {
  constructor(props) {
    super(props);
	this.state = {displayName: 'admin'};
  }

  render() {
    return (
     <Grid>
	    <Header displayName={this.state.displayName}/>
		<Row>
		  <Col md={6}>
			<TaskControl />
		  </Col>
		  <Col md={6}>

		  </Col>
		</Row>
	  </Grid>);
  }
}
