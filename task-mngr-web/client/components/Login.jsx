import React from 'react';
import Bootstrap from 'bootstrap/dist/css/bootstrap.min.css';
import { FormGroup, ControlLabel, FormControl, Button, Grid, Row, Col } from 'react-bootstrap';
import { Link, browserHistory } from 'react-router';
import FieldGroup from './FieldGroup.jsx';

export default class Login extends React.Component {
  constructor(props) {
    super(props);
	this.state = {username: '', password: ''};
	this.usernameChange = this.usernameChange.bind(this);
	this.passwordChange = this.passwordChange.bind(this);
	this.submit = this.submit.bind(this);
  }
  
  usernameChange(e) {
    this.setState({username: e.target.value});
  }
  
  passwordChange(e) {
    this.setState({password: e.target.value});
  }
	
  submit(e) {
	let base64 = btoa(`${this.state.username}:${this.state.password}`);
	let cfg = {  
	  mode: 'no-cors',
      method: 'POST',
      headers: {
        'Accept': 'application/json',
		'Authorization': 'Basic ' + base64
      }
	};
	// Aqui disparariamos a requisição para autenticar usando fetch(),
	// mas o cabeçalho Authorization nunca é enviado.
	// fetch('http://localhost:9090/authenticate', cfg)
	browserHistory.push("/tasks");
  }
	
  render() {
    return (
	  <Grid>
	    <Row>
		  <Col md={4} mdOffset={4}>
			<ControlLabel>Autentique-se para acessar o sistema</ControlLabel>
			<form>
			  <FieldGroup id="input-username" type="text" value={this.state.username} handleChange={this.usernameChange} label="Usuário" placeholder="Digite o seu usuário"/>
			  <FieldGroup id="input-password" type="password" value={this.state.password} handleChange={this.passwordChange} label="Senha" placeholder="Digite a sua senha"/>
			  <Button bsStyle="primary" onClick={this.submit}>Entrar</Button>
			</form>
		  </Col>
		</Row>
	  </Grid>
     );
  }
}