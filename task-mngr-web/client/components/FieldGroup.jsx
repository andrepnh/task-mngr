import React from 'react';
import Bootstrap from 'bootstrap/dist/css/bootstrap.min.css';
import { FormGroup, ControlLabel, FormControl } from 'react-bootstrap';

export default class FieldGroup extends React.Component {
  render() {
	let { id, type, componentClass, label, help, placeholder, value, handleChange } = this.props;
    return (
	  <FormGroup controlId={id}>
        <ControlLabel>{label}</ControlLabel>
	    <FormControl type={type} componentClass={componentClass} value={value} placeholder={placeholder} onChange={handleChange} />
        {help && <HelpBlock>{help}</HelpBlock>}
      </FormGroup>);
  }
}