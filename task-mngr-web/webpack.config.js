const path = require('path');
const webpack = require('webpack');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const htmlWebpackPlugin = new HtmlWebpackPlugin({
  template: './client/index.html',
  filename: 'index.html',
  inject: 'body'
});

const loaderOptionsPlugin = new webpack.LoaderOptionsPlugin({
  options: {
    context: path.join(__dirname, 'client'),
    output: {
      path: path.join(__dirname, 'src/main/resources/assets')
    }
  }
});

module.exports = {
  entry: './client/index.js',
  output: {
    path: path.resolve('src/main/resources/assets'),
    filename: 'index_bundle.js'
  },
  module: {
    loaders: [
      { test: /\.jsx?$/, exclude:/node_modules/, loader :'babel-loader', query: { presets: ['react','es2015'] } },
	  { test: /\.css$/, loader: "style-loader!css-loader" },
	  { test: /\.png$/, loader: "url-loader?limit=100000" },
      { test: /\.jpg$/, loader: "file-loader" },
      { test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/, loader: 'url-loader?limit=10000&mimetype=application/font-woff' },
      { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url-loader?limit=10000&mimetype=application/octet-stream' },
      { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file-loader' },
      { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: 'url-loader?limit=10000&mimetype=image/svg+xml' }
    ]
  },
  plugins: [htmlWebpackPlugin, loaderOptionsPlugin]
};