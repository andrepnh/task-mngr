package andrepnh.task.mngr.rest;

import io.dropwizard.Configuration;
import org.hibernate.validator.constraints.NotEmpty;

public class TaskManagerRestConfiguration extends Configuration {
    @NotEmpty
    private String testUser;
    
    @NotEmpty
    private String testUserDisplayName;

    @NotEmpty
    private String testUserPassword;

    public String getTestUser() {
        return testUser;
    }

    public void setTestUser(String testUser) {
        this.testUser = testUser;
    }

    public String getTestUserPassword() {
        return testUserPassword;
    }

    public void setTestUserPassword(String testUserPassword) {
        this.testUserPassword = testUserPassword;
    }

    public String getTestUserDisplayName() {
        return testUserDisplayName;
    }

    public void setTestUserDisplayName(String testUserDisplayName) {
        this.testUserDisplayName = testUserDisplayName;
    }
    
}
