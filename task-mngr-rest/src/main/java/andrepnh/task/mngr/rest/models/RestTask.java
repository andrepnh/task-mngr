package andrepnh.task.mngr.rest.models;

import andrepnh.task.mngr.model.Task;
import andrepnh.task.mngr.model.User;
import java.time.LocalDateTime;
import static jersey.repackaged.com.google.common.base.Preconditions.*;

public class RestTask {
    private final Task task;

    public RestTask(Task task) {
        this.task = checkNotNull(task);
    }

    public Task.Status getStatus() {
        return task.getStatus();
    }

    public String getUser() {
        return task.getUser().map(User::getUserName).orElse(null);
    }

    public int getCode() {
        return task.getCode();
    }

    public LocalDateTime getCreationDate() {
        return task.getCreationDate();
    }

    public String getDescription() {
        return task.getDescription();
    }

    public String getAnswer() {
        return task.getAnswer().orElse(null);
    }
}
