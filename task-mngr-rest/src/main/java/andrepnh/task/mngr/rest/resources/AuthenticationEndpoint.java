package andrepnh.task.mngr.rest.resources;

import andrepnh.task.mngr.rest.models.Credentials;
import andrepnh.task.mngr.services.AuthenticationService;
import static com.google.common.base.Preconditions.*;
import com.google.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/authentication")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AuthenticationEndpoint {
    private final AuthenticationService service;

    @Inject
    public AuthenticationEndpoint(AuthenticationService service) {
        this.service = checkNotNull(service);
    }
    
    @POST
    public Response authenticate(@NotNull Credentials credentials) {
        service.authenticate(credentials.getUserName(), credentials.getPassword());
        return Response.ok().build();
    }
}
