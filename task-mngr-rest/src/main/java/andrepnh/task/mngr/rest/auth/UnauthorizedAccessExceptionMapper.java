package andrepnh.task.mngr.rest.auth;

import andrepnh.task.mngr.rest.models.JsonError;
import andrepnh.task.mngr.services.UnauthorizedAccessException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.nio.charset.StandardCharsets;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class UnauthorizedAccessExceptionMapper 
    implements ExceptionMapper<UnauthorizedAccessException> {

    @Override
    public Response toResponse(UnauthorizedAccessException e) {
        try {
            JsonError jsonError = converToJsonError(e);
            return Response
                .status(jsonError.getCode())
                .entity(new ObjectMapper().writeValueAsString(jsonError))
                .type(MediaType.APPLICATION_JSON)
                .encoding(StandardCharsets.UTF_8.name())
                .build();
        } catch (JsonProcessingException ex) {
            return Response
                .status(500)
                .entity(ex.getMessage())
                .build();
        }
    }
    
    private JsonError converToJsonError(UnauthorizedAccessException e) {
        return new JsonError(401, e.getMessage());
    }

}
