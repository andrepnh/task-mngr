package andrepnh.task.mngr.rest.auth;

import andrepnh.task.mngr.rest.models.Credentials;
import andrepnh.task.mngr.services.AuthenticationService;
import andrepnh.task.mngr.services.UnauthorizedAccessException;
import com.google.inject.Inject;
import java.io.IOException;
import java.util.Optional;
import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.ext.Provider;
import static jersey.repackaged.com.google.common.base.Preconditions.checkNotNull;
import org.glassfish.jersey.internal.util.Base64;

//@RequiresAuthentication
//@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {
    private final AuthenticationService service;

    @Inject
    public AuthenticationFilter(AuthenticationService service) {
        this.service = checkNotNull(service);
    }
    
    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        String header = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
        
        Credentials credentials = extractBase64(header)
            .flatMap(base64 -> parse(base64))
            .orElseThrow(() -> new UnauthorizedAccessException());
        
        service.authenticate(credentials.getUserName(), credentials.getPassword());
    }
    
    private Optional<String> extractBase64(String authHeader) {
        return Optional.ofNullable(authHeader)
            .filter(header -> header.startsWith("Basic "))
            .map(header -> header.substring("Basic ".length()));
    }

    private Optional<Credentials> parse(String base64) {
        String[] parts = Base64.decodeAsString(base64).split(":", 2);
        if (parts.length != 2) {
            return Optional.empty();
        }
        return Optional.of(new Credentials(parts[0], parts[1]));
    }
}
