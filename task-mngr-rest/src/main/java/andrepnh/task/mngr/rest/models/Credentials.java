package andrepnh.task.mngr.rest.models;

import static andrepnh.task.mngr.model.InputPreconditions.*;
import com.fasterxml.jackson.annotation.JsonCreator;

public class Credentials {
    private final String userName;
    
    private final String password;

    @JsonCreator
    public Credentials(String userName, String password) {
        String trimmedUsername = validateNotNull(userName, "O usuário é obrigatório").trim(),
            trimmedPassword = validateNotNull(password, "A senha é obrigatória").trim();
        validateArgument(!trimmedUsername.isEmpty(),
            "O usuário não pode ser vazio ou apenas whitespace; obteve %s",
            userName);
        validateArgument(!trimmedPassword.isEmpty(), "A senha é obrigatória");
        this.userName = trimmedUsername;
        this.password = trimmedPassword;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }
    
}
