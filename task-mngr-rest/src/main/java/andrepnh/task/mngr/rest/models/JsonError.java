package andrepnh.task.mngr.rest.models;

public class JsonError {
    
    private final int code;
    private final String message;

    public JsonError(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
    
}
