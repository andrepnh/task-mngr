package andrepnh.task.mngr.rest.models;

import andrepnh.task.mngr.rest.models.JsonError;

public class RestException extends RuntimeException {
    private final JsonError error;
    
    public RestException(int code, String message) {
        this.error = new JsonError(code, message);
    }
    
    public RestException(int code, String message, Object... args) {
        this.error = new JsonError(code, String.format(message, args));
    }

    public JsonError getError() {
        return error;
    }
    
}
