package andrepnh.task.mngr.rest.resources;

import andrepnh.task.mngr.rest.models.RestException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.nio.charset.StandardCharsets;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class RestExceptionMapper implements ExceptionMapper<RestException> {

    @Override
    public Response toResponse(RestException e) {
        try {
            return Response
                .status(e.getError().getCode())
                .entity(new ObjectMapper().writeValueAsString(e.getError()))
                .type(MediaType.APPLICATION_JSON)
                .encoding(StandardCharsets.UTF_8.name())
                .build();
        } catch (JsonProcessingException ex) {
            return Response
                .status(500)
                .entity(ex.getMessage())
                .build();
        }
    }
    
}
