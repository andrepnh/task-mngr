package andrepnh.task.mngr.rest.models;

/**
 * Tournou-se necessário porque o fetch do javascript não lidava bem com enviar
 * um JSON que era apenas uma string
 */
public class Description {
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
