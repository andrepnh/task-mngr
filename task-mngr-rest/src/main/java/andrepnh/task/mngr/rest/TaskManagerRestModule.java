package andrepnh.task.mngr.rest;

import andrepnh.task.mngr.repositories.TaskRepository;
import andrepnh.task.mngr.repositories.UserRepository;
import andrepnh.task.mngr.repositories.impl.MemoryDatabase;
import andrepnh.task.mngr.repositories.impl.MemoryTaskRepository;
import andrepnh.task.mngr.repositories.impl.MemoryUserRepository;
import com.google.inject.Binder;
import com.google.inject.Module;

public class TaskManagerRestModule implements Module {

    @Override
    public void configure(Binder binder) {
        binder.bind(MemoryDatabase.class).toInstance(new MemoryDatabase());
        binder.bind(TaskRepository.class).to(MemoryTaskRepository.class);
        binder.bind(UserRepository.class).to(MemoryUserRepository.class);
    }
    
}
