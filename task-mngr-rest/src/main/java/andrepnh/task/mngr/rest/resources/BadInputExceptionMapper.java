package andrepnh.task.mngr.rest.resources;

import andrepnh.task.mngr.rest.models.JsonError;
import andrepnh.task.mngr.model.BadInputException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.nio.charset.StandardCharsets;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class BadInputExceptionMapper implements ExceptionMapper<BadInputException> {

    @Override
    public Response toResponse(BadInputException e) {
        try {
            JsonError jsonError = converToJsonError(e);
            return Response
                .status(jsonError.getCode())
                .entity(new ObjectMapper().writeValueAsString(jsonError))
                .type(MediaType.APPLICATION_JSON)
                .encoding(StandardCharsets.UTF_8.name())
                .build();
        } catch (JsonProcessingException ex) {
            return Response
                .status(500)
                .entity(ex.getMessage())
                .build();
        }
    }
    
    private JsonError converToJsonError(BadInputException e) {
        int statusCode = convertToStatusCode(e.getErrorType());
        return new JsonError(statusCode, e.getMessage());
    }

    private int convertToStatusCode(BadInputException.ErrorType errorType) {
        switch (errorType) {
            case BAD_INPUT: return 412; // Precondition Failed
            case BAD_STATE: return 412; // Precondition Failed
            case MISSING_INPUT: return 404;
            default: throw new IllegalArgumentException("ErrorType desconhecido: " + errorType);
        }
    }

}
