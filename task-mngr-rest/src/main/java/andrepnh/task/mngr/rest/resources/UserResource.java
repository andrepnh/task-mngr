package andrepnh.task.mngr.rest.resources;

import andrepnh.task.mngr.rest.models.RestException;
import andrepnh.task.mngr.model.Task;
import andrepnh.task.mngr.model.User;
import andrepnh.task.mngr.rest.models.Description;
import andrepnh.task.mngr.rest.models.RestTask;
import andrepnh.task.mngr.services.UserService;
import andrepnh.task.mngr.services.TaskService;
import static com.google.common.base.Preconditions.*;
import com.google.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/users")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
// @RequiresAuthentication
public class UserResource {
    private final UserService userService;
    
    private final TaskService taskService;

    @Inject
    public UserResource(UserService userService, TaskService taskService) {
        this.userService = checkNotNull(userService);
        this.taskService = checkNotNull(taskService);
    }
    
    @POST
    @Path("{username}/tasks")
    public RestTask createTask(@PathParam("username") String username, Description description) {
        User user = getUser(username);
        RestTask task = new RestTask(userService.createTask(user, description.getDescription()));
        return task;
    }
    
    @PUT
    @Path("{username}/tasks/{code}/complete")
    public RestTask completeTask(
        @PathParam("username") String username, 
        @PathParam("code") int code,
        String answer) {
        User user = getUser(username);
        RestTask task = new RestTask(
            userService.completeTask(user, getTask(code), answer));
        return task;
    }
    
    @PUT
    @Path("{username}/tasks/{code}/take")
    public RestTask takeTask(
        @PathParam("username") String username, 
        @PathParam("code") int code) {
        User user = getUser(username);
        RestTask task = new RestTask(
            userService.takeTask(user, getTask(code)));
        return task;
    }
    
    @PUT
    @Path("{username}/tasks/{code}/drop")
    public RestTask dropTask(
        @PathParam("username") String username, 
        @PathParam("code") int code) {
        User user = getUser(username);
        RestTask task = new RestTask(
            userService.dropTask(user, getTask(code)));
        return task;
    }
    
    private User getUser(String username) {
        return userService.findById(username)
            .orElseThrow(() -> new RestException(404, "Usuário %s não encontrado", username));
    }
    
    private Task getTask(int code) {
        return taskService.findById(code)
            .orElseThrow(() -> new RestException(404, "Tarefa %d não encontrada", code));
    }
}
