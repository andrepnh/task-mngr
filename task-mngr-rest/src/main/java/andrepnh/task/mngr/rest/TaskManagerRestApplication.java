package andrepnh.task.mngr.rest;

import andrepnh.task.mngr.model.Task;
import andrepnh.task.mngr.model.User;
import andrepnh.task.mngr.rest.resources.BadInputExceptionMapper;
import andrepnh.task.mngr.rest.resources.RestExceptionMapper;
import andrepnh.task.mngr.rest.auth.UnauthorizedAccessExceptionMapper;
import andrepnh.task.mngr.services.UserService;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.hubspot.dropwizard.guice.GuiceBundle;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class TaskManagerRestApplication extends Application<TaskManagerRestConfiguration> {

    private GuiceBundle<TaskManagerRestConfiguration> guiceBundle;

    public static void main(String[] args) throws Exception {
        new TaskManagerRestApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<TaskManagerRestConfiguration> bootstrap) {
        guiceBundle = GuiceBundle.<TaskManagerRestConfiguration>newBuilder()
            .addModule(new TaskManagerRestModule())
            .enableAutoConfig(
                "andrepnh.task.mngr.rest", 
                "andrepnh.task.mngr.services")
            .setConfigClass(TaskManagerRestConfiguration.class)
            .build();
        
        bootstrap.addBundle(guiceBundle);
    }
    
    @Override
    public void run(TaskManagerRestConfiguration cfg, Environment env) throws Exception {
        env.jersey().register(new RestExceptionMapper());
        env.jersey().register(new BadInputExceptionMapper());
        env.jersey().register(new UnauthorizedAccessExceptionMapper());
        // Para formatação de datas como ISO8601
        env.getObjectMapper().disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        UserService userService = guiceBundle.getInjector().getInstance(UserService.class);
        insertTestData(userService);
        
    }

    @Override
    public String getName() {
        return "task-mnrg-rest";
    }

    private void insertTestData(UserService userService) {
        User joao = userService.createUser("joao", "João Pedro", "joao"),
            guilherme = userService.createUser("guilherme", "Guilhere Santos", "guilherme");
        Task task1 = userService.createTask(joao, "Realizar a extraçao do relatório mensal de clientes no sistema de relatórios e enviar os nomes dos inadimplentes."),
            task2 = userService.createTask(joao, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce convallis pellentesquemetus id lacinia."),
            task3 = userService.createTask(guilherme, "Nunc dapibus pulvinar auctor. Duis nec sem at orci commodo vierra id in ipsum."),
            task4 = userService.createTask(joao, "Lorem ipsum dolor sit amet, consectetur adipscing elit.");
        userService.dropTask(joao, task2);
        userService.dropTask(joao, task4);
    }

}
