package andrepnh.task.mngr.rest.resources;

import andrepnh.task.mngr.rest.auth.RequiresAuthentication;
import andrepnh.task.mngr.rest.models.RestException;
import andrepnh.task.mngr.rest.models.RestTask;
import andrepnh.task.mngr.services.TaskService;
import com.google.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import static jersey.repackaged.com.google.common.base.Preconditions.checkNotNull;

@Path("/tasks")
@Produces(MediaType.APPLICATION_JSON)
// @RequiresAuthentication
public class TaskResource {
    private final TaskService service;

    @Inject
    public TaskResource(TaskService service) {
        this.service = checkNotNull(service);
    }
    
    @GET
    public List<RestTask> getTasks() {
        return service.findAll().stream()
            .map(RestTask::new)
            .collect(Collectors.toList());
    }
    
    @GET
    @Path("{code}")
    public RestTask getTask(@PathParam("code") int code) {
        return service.findById(code)
            .map(RestTask::new)
            .orElseThrow(() -> new RestException(404, "Tarefa %d não encontrada", code));
    }
}
