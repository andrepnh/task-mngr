package andrepnh.task.mngr.rest.resources;

import andrepnh.task.mngr.rest.models.RestException;

public final class RestPreconditions {
    private RestPreconditions() {
    }
    
    public static void check(boolean condition, int errorStatusCode, String jsonMessage) {
        if (!condition) {
            throw new RestException(errorStatusCode, jsonMessage);
        }
    }
    
    public static void check(boolean condition, int errorStatusCode, 
        String jsonMessage, Object... args) {
        if (!condition) {
            throw new RestException(errorStatusCode, String.format(jsonMessage, args));
        }
    }
}
