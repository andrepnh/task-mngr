package andrepnh.task.mngr.test.utils;

import java.util.function.Consumer;
import static org.junit.Assert.*;

public final class ExtendedAssertions {

    private ExtendedAssertions() {
    }

    public static <E extends Exception> void assertThrows(
        Runnable action, String message, Class<E> exceptionClass) {
        ExtendedAssertions.<E>assertThrows(action, message, exceptionClass, (ex) -> {});
    }
    
    public static <E extends Exception> void assertThrows(
        Runnable action, String message, Class<E> exceptionClass, Consumer<E> assertions) {
        try {
            action.run();
            fail(message);
        } catch (Exception ex) {
            if (!exceptionClass.isInstance(ex)) {
                // Diferentemente do JUnit, lançamos RuntimeException ao invés de AssertionError
                // para mantes o stacktrace da exceção original
                throw new RuntimeException(
                    String.format("Esperava %s, obteve %s", exceptionClass, ex),
                    ex);
            }
            assertions.accept(exceptionClass.cast(ex));
        }
    }

}
