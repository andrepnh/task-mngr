package andrepnh.task.mngr.model;

import andrepnh.task.mngr.model.BadInputException.ErrorType;
import static andrepnh.task.mngr.test.utils.ExtendedAssertions.*;
import static org.junit.Assert.*;
import org.junit.Test;

public class TaskTest {
    private final Task pendingTask;
    
    private final Task takenTask;
    
    private final Task completedTask;
    
    private final User fulano;

    public TaskTest() {
        this.pendingTask = new Task("Alguma tarefa");
        
        this.takenTask = new Task("Outra tarefa");
        this.fulano = new User("fulano", "Fulano", "abc");
        takenTask.markAsTaken(fulano);
        
        this.completedTask = new Task("Ainda outra tarefa");
        completedTask.markAsTaken(fulano);
        completedTask.markAsCompleted(fulano, "Pronto");
    }
    
    @Test
    public void mustHaveADescription() {
        assertThrows(() -> new Task(null), 
            "Uma tarefa não pode ter descrição nula",
            BadInputException.class,
            ex -> assertEquals(ErrorType.MISSING_INPUT, ex.getErrorType()));
        
        assertThrows(() -> new Task(" \n "), 
            "Uma tarefa não pode ter descrição vazia ou apenas whitespace",
            BadInputException.class,
            ex -> assertEquals(ErrorType.BAD_INPUT, ex.getErrorType()));
    }
    
    @Test
    public void onlyAPendingTaskCanBeTaken() {
        pendingTask.markAsTaken(fulano);
        assertEquals(Task.Status.TAKEN, pendingTask.getStatus());
        
        assertThrows(() -> takenTask.markAsTaken(fulano), 
            "Uma tarefa pegada não pode ser reatribuída", 
            BadInputException.class,
            ex -> assertEquals(ErrorType.BAD_STATE, ex.getErrorType()));
        
        assertThrows(() -> completedTask.markAsTaken(fulano), 
            "Uma tarefa completada não pode ser pega", 
            BadInputException.class,
            ex -> assertEquals(ErrorType.BAD_STATE, ex.getErrorType()));
    }
    
    @Test
    public void onlyATakenTaskCanBeCompleted() {
        takenTask.markAsCompleted(fulano, "Ok");
        assertEquals(Task.Status.COMPLETED, takenTask.getStatus());
        
        assertThrows(() -> pendingTask.markAsCompleted(fulano, "ok"), 
            "Uma tarefa pendente não pode ser completada", 
            BadInputException.class,
            ex -> assertEquals(ErrorType.BAD_STATE, ex.getErrorType()));
        
        assertThrows(() -> completedTask.markAsCompleted(fulano, "ok"), 
            "Uma tarefa completada não pode ser completada novamente", 
            BadInputException.class,
            ex -> assertEquals(ErrorType.BAD_STATE, ex.getErrorType()));
    }
    
    @Test
    public void onlyATakenOrPendentTaskCanBeMarkedAsPending() {
        takenTask.markAsPending();
        assertEquals(Task.Status.PENDING, takenTask.getStatus());
        
        pendingTask.markAsPending();
        assertEquals(Task.Status.PENDING, pendingTask.getStatus());
        
        assertThrows(() -> completedTask.markAsPending(), 
            "Uma tarefa completada não pode ser marcada como pendente", 
            BadInputException.class,
            ex -> assertEquals(ErrorType.BAD_STATE, ex.getErrorType()));
    }
    
    @Test(expected = NullPointerException.class)
    public void cannotBeTakenByNullUser() {
        pendingTask.markAsTaken(null);
    }
    
    @Test(expected = NullPointerException.class)
    public void cannotBeCompletedByNullUser() {
        takenTask.markAsCompleted(null, "ok");
    }
    
    @Test
    public void canBeCompleteWithANullAnswer() {
        takenTask.markAsCompleted(fulano, null);
    }
}
