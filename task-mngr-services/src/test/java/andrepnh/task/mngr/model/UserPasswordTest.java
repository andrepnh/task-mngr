package andrepnh.task.mngr.model;

import static andrepnh.task.mngr.test.utils.ExtendedAssertions.*;
import org.junit.Test;

public class UserPasswordTest {
    private final User john = new User("john", "John", "abc");
    
    @Test
    public void mustHaveHash() {
        assertThrows(() -> new UserPassword(john, null),
            "Não deveria aceitar hash nulo",
            NullPointerException.class);
        
        assertThrows(() -> new UserPassword(john, "  \n "),
            "Não deveria aceitar hash vaziou ou só whitespace",
            IllegalArgumentException.class);
    }
    
    @Test(expected = NullPointerException.class)
    public void mustHaveUser() {
        new UserPassword(null, "abc");
    }
}
