package andrepnh.task.mngr.model;

import static andrepnh.task.mngr.test.utils.ExtendedAssertions.*;
import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;

public class UserTest {
    
    private final User john;
    
    private final Task doTheDishes;
    
    public UserTest() {
        john = new User("john", "John Doe", "abc");
        doTheDishes = new Task("Do the dishes");
    }
    
    @Test
    public void mustHaveAPassword() {
        assertThrows(() -> new User("john", "Fulano", null), 
            "Um usuário não pode ter senha nula",
            BadInputException.class,
            ex -> assertEquals(BadInputException.ErrorType.MISSING_INPUT, ex.getErrorType()));
        
        assertThrows(() -> new User("john", "Fulano", ""), 
            "Um usuário não pode ter senha vazia",
            BadInputException.class,
            ex -> assertEquals(BadInputException.ErrorType.BAD_INPUT, ex.getErrorType()));
    }
    
    @Test
    public void mustHaveAnUserName() {
        assertThrows(() -> new User(null, "Fulano", "abc"), 
            "Um usuário não pode ter username nulo",
            BadInputException.class,
            ex -> assertEquals(BadInputException.ErrorType.MISSING_INPUT, ex.getErrorType()));
        
        assertThrows(() -> new User(" \n ", "Fulano", "abc"), 
            "Um usuário não pode ter usernam vazio ou apenas whitespace",
            BadInputException.class,
            ex -> assertEquals(BadInputException.ErrorType.BAD_INPUT, ex.getErrorType()));
    }
    
    @Test
    public void mustHaveADisplayName() {
        assertThrows(() -> new User("fulano", null, "abc"), 
            "O nome do usuário não pode ser nulo",
            BadInputException.class,
            ex -> assertEquals(BadInputException.ErrorType.MISSING_INPUT, ex.getErrorType()));
        
        assertThrows(() -> new User("fulano", " \n ", "abc"), 
            "O nome do usuário não pode ser vazio ou apenas whitespace",
            BadInputException.class,
            ex -> assertEquals(BadInputException.ErrorType.BAD_INPUT, ex.getErrorType()));
    }
    
    @Test
    public void changingTheReturnedTaskListShouldNotReflectOnUser() {
        john.getTasksTaken().add(doTheDishes);
        assertTrue(john.getTasksTaken().isEmpty());
    }
    
    @Test(expected = NullPointerException.class)
    public void shouldNotTakeANullTask() {
        john.takeTask(null);
    }
    
    @Test
    public void shouldNotTakeAnAlreadyAssignedTask() {
        john.takeTask(doTheDishes);
        User jane = new User("jane", "Jane Doe", "abc");
        assertThrows(() -> jane.takeTask(doTheDishes), 
            "Um usuário não pode pegar uma tarefa já atribuída", 
            BadInputException.class,
            ex -> assertEquals(BadInputException.ErrorType.BAD_STATE, ex.getErrorType()));
    }
    
    @Test
    public void takingATaskUpdatesTaskListAndTaskAsignee() {
        john.takeTask(doTheDishes);
        assertTrue(john.getTasksTaken().contains(doTheDishes));
        assertEquals(john, doTheDishes.getTakenBy().orElse(null));
    }
    
    @Test(expected = NullPointerException.class)
    public void shouldNotDropANullTask() {
        john.dropTask(null);
    }
    
    @Test
    public void shouldNotDropAnUnassignedTask() {
        assertThrows(() -> john.dropTask(doTheDishes),
            "Uma tarefa não atribuída não pode ser largada",
            BadInputException.class,
            ex -> assertEquals(BadInputException.ErrorType.BAD_INPUT, ex.getErrorType()));
    }
    
    @Test
    public void shouldNotDropATaskAssignedToSomeoneElse() {
        john.takeTask(doTheDishes);
        User jane = new User("jane", "Jane", "abc");
        assertThrows(() -> jane.dropTask(doTheDishes), 
            "Um usuário não pode largar uma tarefa atribuída a outro", 
            BadInputException.class,
            ex -> assertEquals(BadInputException.ErrorType.BAD_INPUT, ex.getErrorType()));
    }
    
    @Test
    public void dropingATaskRemovesItFromTaskListAndResetsTaskAsignee() {
        john.takeTask(doTheDishes);
        john.dropTask(doTheDishes);
        assertEquals(new ArrayList<>(), john.getTasksTaken());
        assertNull(doTheDishes.getTakenBy().orElse(null));
    }
    
    @Test(expected = NullPointerException.class)
    public void shouldNotCompleteANullTask() {
        john.completeTask(null, "ok");
    }
    
    @Test
    public void shouldNotCompleteAnUnassignedTask() {
        assertThrows(() -> john.completeTask(doTheDishes, "ok"),
            "Uma tarefa não atribuída não pode ser completada",
            BadInputException.class,
            ex -> assertEquals(BadInputException.ErrorType.BAD_INPUT, ex.getErrorType()));
    }
    
    @Test
    public void shouldNotCompleteATaskAssignedToSomeoneElse() {
        john.takeTask(doTheDishes);
        john.completeTask(doTheDishes, "ok");
        User jane = new User("jane", "Jane", "abc");
        assertThrows(() -> jane.completeTask(doTheDishes, "ok"), 
            "Um usuário não pode completar a tarefa de outro", 
            BadInputException.class,
            ex -> assertEquals(BadInputException.ErrorType.BAD_INPUT, ex.getErrorType()));
    }
    
    @Test
    public void completingATaskRemovesItFromTaskListAndMarksItAsCompleted() {
        john.takeTask(doTheDishes);
        john.completeTask(doTheDishes, "ok");
        assertEquals(new ArrayList<>(), john.getTasksTaken());
        assertEquals(john, doTheDishes.getCompletedBy().orElse(null));
    }
}
