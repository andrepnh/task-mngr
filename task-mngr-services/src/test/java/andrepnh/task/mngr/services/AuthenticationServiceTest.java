package andrepnh.task.mngr.services;

import andrepnh.task.mngr.model.User;
import andrepnh.task.mngr.repositories.UserRepository;
import andrepnh.task.mngr.repositories.impl.MemoryDatabase;
import andrepnh.task.mngr.repositories.impl.MemoryTaskRepository;
import andrepnh.task.mngr.repositories.impl.MemoryUserRepository;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static andrepnh.task.mngr.test.utils.ExtendedAssertions.assertThrows;

public class AuthenticationServiceTest {
    private final AuthenticationService service;
    private final User john;
    private final String johnPlainPassword = "asdasde";

    public AuthenticationServiceTest() {
        MemoryDatabase db = new MemoryDatabase();
        UserRepository userRepository = new MemoryUserRepository(db);
        service = new AuthenticationService(userRepository);
        UserService userService = new UserService(
            service, userRepository, new MemoryTaskRepository(db));
        john = userService.createUser("john", "John", johnPlainPassword);
    }
 
    @Test
    public void differentPasswordsShouldNotMatch() {
        String differentPassword = johnPlainPassword + "1";
        assertThrows(
            () -> service.authenticate(john.getUserName(), differentPassword),
            String.format("As senhas %s e %s são diferentes, mas foram consideradas iguais",
                johnPlainPassword, differentPassword),
            UnauthorizedAccessException.class,
            (ex) -> assertEquals(john.getUserName(), ex.getUsername()));
    }
    
    @Test
    public void equalPasswordsShouldMatch() {
        service.authenticate(john.getUserName(), johnPlainPassword);
    }
    
    @Test
    public void usersShouldNotBeAbleToAuthenticateByPassingTheHash() {
        assertThrows(
            () -> service.authenticate(john.getUserName(), john.getHashedPassword()),
            "Um usuário não deveria ser autenticado ao passar seu hash como senha",
            UnauthorizedAccessException.class,
            (ex) -> assertEquals(john.getUserName(), ex.getUsername()));
    }
}
