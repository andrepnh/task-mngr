package andrepnh.task.mngr.services;

import andrepnh.task.mngr.model.Task;
import andrepnh.task.mngr.model.User;
import andrepnh.task.mngr.repositories.TaskRepository;
import andrepnh.task.mngr.repositories.UserRepository;
import andrepnh.task.mngr.repositories.impl.MemoryDatabase;
import andrepnh.task.mngr.repositories.impl.MemoryTaskRepository;
import andrepnh.task.mngr.repositories.impl.MemoryUserRepository;
import static org.junit.Assert.*;
import org.junit.Test;

public class UserServiceTest {
    private final UserService userService;
    
    private final UserRepository userRepository;
    
    private final TaskRepository taskRepository;
    
    private final User john;
            
    public UserServiceTest() {
        MemoryDatabase db = new MemoryDatabase();
        userRepository = new MemoryUserRepository(db);
        taskRepository = new MemoryTaskRepository(db);
        this.userService = new UserService(
            new AuthenticationService(new MemoryUserRepository(db)),
            userRepository,
            taskRepository);
        john = userService.createUser("john", "John", "asdasda");
    }
    
    @Test
    public void createdUsersMustHaveHashedPasswords() {
        String password = "asdasd";
        User user = userService.createUser("a", "b", password);
        assertNotEquals(password, user.getHashedPassword());
    }
    
    @Test
    public void createdUsersArePersisted() {
        assertEquals(john, userRepository.findById(john.getId()).orElse(null));
    }
    
    @Test
    public void aUserAutomaticallyTakesATaskAfterCreatingIt() {
        Task task = userService.createTask(john, "foo");
        assertEquals(Task.Status.TAKEN, task.getStatus());
        assertTrue(john.getTasksTaken().contains(task));
    }
    
    @Test
    public void dropingATaskReflectsOnPersistedUserAndTask() {
        Task task = userService.createTask(john, "foo");
        userService.dropTask(john, task);
        User persistedUser = userRepository.findById(john.getId()).get();
        Task persistedTask = taskRepository.findById(task.getId()).get();
        assertEquals(Task.Status.PENDING, persistedTask.getStatus());
        assertFalse(persistedUser.getTasksTaken().contains(task));
    }
    
    @Test
    public void takingATaskReflectsOnPersistedUserAndTask() {
        Task task = userService.createTask(john, "foo");
        User persistedUser = userRepository.findById(john.getId()).get();
        Task persistedTask = taskRepository.findById(task.getId()).get();
        assertEquals(Task.Status.TAKEN, persistedTask.getStatus());
        assertTrue(persistedUser.getTasksTaken().contains(task));
    }
    
    @Test
    public void completingATaskReflectsOnPersistedUserAndTask() {
        Task task = userService.createTask(john, "foo");
        String answer = "bar";
        userService.completeTask(john, task, answer);
        User persistedUser = userRepository.findById(john.getId()).get();
        Task persistedTask = taskRepository.findById(task.getId()).get();
        assertEquals(Task.Status.COMPLETED, persistedTask.getStatus());
        assertEquals(answer, persistedTask.getAnswer().orElse(null));
        assertFalse(persistedUser.getTasksTaken().contains(task));
    }
    
}
