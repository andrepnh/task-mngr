package andrepnh.task.mngr.repositories.impl;

import andrepnh.task.mngr.model.User;
import java.util.ArrayList;
import java.util.Optional;
import static org.junit.Assert.*;
import org.junit.Test;

public class MemoryRepositoryTest {
    private final MemoryRepository<User, String> repository;

    public MemoryRepositoryTest() {
        repository = new MemoryRepository<>(new MemoryDatabase(), User.class);
    }
    
    @Test(expected = NullPointerException.class)
    public void mustHaveADatabase() {
        new MemoryRepository<>(null, User.class);
    }
    
    @Test(expected = NullPointerException.class)
    public void cannotInsertNullEntity() {
        repository.insert(null);
    }
    
    @Test(expected = NullPointerException.class)
    public void cannotUpdateNullEntity() {
        repository.update(null);
    }
    
    @Test(expected = NullPointerException.class)
    public void cannotFindByNullId() {
        repository.findById(null);
    }
    
    @Test
    public void shouldReturnAnEmptyOptionalWhenEntityIsntFoundById() {
        assertEquals(Optional.empty(), repository.findById("a"));
    }
    
    @Test
    public void shouldReturnAnEmptyListWhenThereAreNoEntities() {
        assertEquals(new ArrayList<>(), repository.findAll());
    }
    
    @Test
    public void addingAnExistingEntityHasNoEffect() {
        User john1 = new User("john", "John", "abc"), 
            john2 = new User(john1.getUserName(), "John Doe", "abc");
        repository.insert(john1);
        repository.insert(john2);
        User persisted = repository.findById(john1.getId()).get();
        assertNotEquals(john2.getDisplayName(), persisted.getDisplayName());
    }
    
    @Test
    public void updatingAnEntityReplacesIt() {
        User john1 = new User("john", "John", "abc"), 
            john2 = new User(john1.getUserName(), "John Doe", "abc");
        repository.insert(john1);
        repository.update(john2);
        User persisted = repository.findById(john1.getId()).get();
        assertEquals(john2.getDisplayName(), persisted.getDisplayName());
    }

}
