package andrepnh.task.mngr.repositories;

import andrepnh.task.mngr.model.UserPassword;
import java.util.UUID;

public interface UserPasswordRepository extends Repository<UserPassword, UUID>{
    
}
