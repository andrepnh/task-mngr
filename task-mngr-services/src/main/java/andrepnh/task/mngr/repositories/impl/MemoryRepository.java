package andrepnh.task.mngr.repositories.impl;

import andrepnh.task.mngr.model.Entity;
import andrepnh.task.mngr.repositories.Repository;
import static com.google.common.base.Preconditions.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Base para repositórios em memória
 * @param <E> tipo da entidade
 * @param <I> tipo do id da entidade
 */
final class MemoryRepository<E extends Entity<I>, I> implements Repository<E, I> {
    // Repositórios tem acesso ao banco de dados inteiro que seja possível fazer
    // joins
    private final MemoryDatabase db;
    
    private final Class<E> entityClass;

    public MemoryRepository(MemoryDatabase db, Class<E> entityClass) {
        this.db = checkNotNull(db);
        this.entityClass = checkNotNull(entityClass);
    }

    @Override
    public E insert(E entity) {
        db.getSet(entityClass).add(checkNotNull(entity, "Entidade nula"));
        return entity;
    }

    @Override
    public E update(E entity) {
        Set<E> set = db.getSet(entityClass);
        set.remove(checkNotNull(entity, "Entidade nula"));
        set.add(entity);
        return entity;
    }

    @Override
    public Optional<E> findById(I id) {
        checkNotNull(id, "id nulo");
        return db.getSet(entityClass).stream()
            .filter(entity -> entity.getId().equals(id))
            .findFirst();
    }

    @Override
    public List<E> findAll() {
        return new ArrayList<>(db.getSet(entityClass));
    }
    
}
