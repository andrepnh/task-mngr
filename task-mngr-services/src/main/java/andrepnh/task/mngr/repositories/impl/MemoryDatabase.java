package andrepnh.task.mngr.repositories.impl;

import andrepnh.task.mngr.model.Entity;
import andrepnh.task.mngr.model.Task;
import andrepnh.task.mngr.model.User;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class MemoryDatabase {
    private final Set<User> users = ConcurrentHashMap.newKeySet();
    
    private final Set<Task> tasks = ConcurrentHashMap.newKeySet();

    Set<User> getUsers() {
        return users;
    }

    Set<Task> getTasks() {
        return tasks;
    }
    
    <E extends Entity<?>> Set<E> getSet(Class<E> entityClass) {
        if (entityClass == User.class) {
            return (Set<E>) getUsers();
        } else if (entityClass == Task.class) {
            return (Set<E>) getTasks();
        } else {
            throw new IllegalArgumentException("Classe de entidade desconhecida: "
                + entityClass.getName());
        }
    }
}
