package andrepnh.task.mngr.repositories;

import andrepnh.task.mngr.model.Entity;
import java.util.List;
import java.util.Optional;

/**
 * Interface comum para fontes de entidades
 * @param <E> tipo da entidade
 * @param <I> tipo da chave primária da entidade
 */
public interface Queryable<E extends Entity<I>, I> {
    Optional<E> findById(I id);
    
    List<E> findAll();
}
