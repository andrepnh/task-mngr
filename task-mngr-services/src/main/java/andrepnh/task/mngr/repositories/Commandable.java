package andrepnh.task.mngr.repositories;

import andrepnh.task.mngr.model.Entity;

/**
 * Interface comum para mutações no estado de entidades
 * @param <E> tipo da entidade
 * @param <I> tipo do id da entidade
 */
public interface Commandable<E extends Entity<I>, I> {
    /**
     * Insere uma nova entidade
     * @return a entidade como agora armazenada
     */
    public E insert(E entity);
    
    /**
     * Atualiza uma entidade
     * @return a entidade atualizada
     */
    public E update(E entity);
}
