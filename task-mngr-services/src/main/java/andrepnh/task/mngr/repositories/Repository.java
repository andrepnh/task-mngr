package andrepnh.task.mngr.repositories;

import andrepnh.task.mngr.model.Entity;

/**
 * Responsável por buscar e atualizar entidades, implementando {@link Queryable}
 * e {@link Commandable}. Essa separação de interfaces segue o interface segregation
 * principle e dá a possibilidade de usar CQRS posteriormente.
 * @param <E> tipo da entidade
 * @param <I> tipo da chave primária da entidade
 */
public interface Repository<E extends Entity<I>, I> 
    extends Commandable<E, I>, Queryable<E, I> {
    
}
