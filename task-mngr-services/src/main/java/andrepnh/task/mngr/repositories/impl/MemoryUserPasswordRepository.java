package andrepnh.task.mngr.repositories.impl;

import andrepnh.task.mngr.model.UserPassword;
import andrepnh.task.mngr.repositories.UserPasswordRepository;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class MemoryUserPasswordRepository implements UserPasswordRepository {
    private final MemoryRepository<UserPassword, UUID> delegate;
    
    public MemoryUserPasswordRepository(MemoryDatabase db) {
        delegate = new MemoryRepository<>(db, UserPassword.class);
    }

    @Override
    public UserPassword insert(UserPassword entity) {
        return delegate.insert(entity);
    }

    @Override
    public UserPassword update(UserPassword entity) {
        return delegate.update(entity);
    }

    @Override
    public Optional<UserPassword> findById(UUID id) {
        return delegate.findById(id);
    }

    @Override
    public List<UserPassword> findAll() {
        return delegate.findAll();
    }
 
    
}
