package andrepnh.task.mngr.repositories;

import andrepnh.task.mngr.model.User;

public interface UserRepository extends Repository<User, String> {
    
}
