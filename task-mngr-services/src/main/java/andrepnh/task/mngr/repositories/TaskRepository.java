package andrepnh.task.mngr.repositories;

import andrepnh.task.mngr.model.Task;

public interface TaskRepository extends Repository<Task, Integer> {
    
}
