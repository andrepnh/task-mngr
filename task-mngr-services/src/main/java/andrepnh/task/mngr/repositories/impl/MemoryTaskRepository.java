package andrepnh.task.mngr.repositories.impl;

import andrepnh.task.mngr.model.Task;
import andrepnh.task.mngr.repositories.TaskRepository;
import com.google.inject.Inject;
import java.util.List;
import java.util.Optional;

public class MemoryTaskRepository implements TaskRepository {
    // Favorecendo composição sobre herança
    private final MemoryRepository<Task, Integer> delegate;

    @Inject
    public MemoryTaskRepository(MemoryDatabase db) {
        delegate = new MemoryRepository<>(db, Task.class);
    }

    @Override
    public Task insert(Task entity) {
        return delegate.insert(entity);
    }

    @Override
    public Task update(Task entity) {
        return delegate.update(entity);
    }

    @Override
    public Optional<Task> findById(Integer id) {
        return delegate.findById(id);
    }

    @Override
    public List<Task> findAll() {
        return delegate.findAll();
    }
    
}
