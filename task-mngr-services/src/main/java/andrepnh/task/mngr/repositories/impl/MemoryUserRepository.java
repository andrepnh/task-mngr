package andrepnh.task.mngr.repositories.impl;

import andrepnh.task.mngr.model.User;
import andrepnh.task.mngr.repositories.UserRepository;
import com.google.inject.Inject;
import java.util.List;
import java.util.Optional;

public class MemoryUserRepository implements UserRepository {
    // Favorecendo composição sobre herança
    private final MemoryRepository<User, String> delegate;

    @Inject
    public MemoryUserRepository(MemoryDatabase db) {
        delegate = new MemoryRepository<>(db, User.class);
    }

    @Override
    public User insert(User entity) {
        return delegate.insert(entity);
    }

    @Override
    public User update(User entity) {
        return delegate.update(entity);
    }

    @Override
    public Optional<User> findById(String userName) {
        return delegate.findById(userName);
    }

    @Override
    public List<User> findAll() {
        return delegate.findAll();
    }
}
