package andrepnh.task.mngr.model;

/**
 * Exceção genérica para erros causados por entradas de terceiros, ao invés
 * de falhas da aplicação.
 * Clientes de métodos que lançam essa exceção podem tratar a mensagem como
 * sendo destinada ao usuário ou sistema que enviou o dado inválido.
 */
public class BadInputException extends RuntimeException {
    private final ErrorType errorType;
    
    public BadInputException(String message, ErrorType errorType) {
        super(message);
        this.errorType = errorType;
    }
    
    public BadInputException(String message, ErrorType cause, Object... args) {
        super(String.format(message, args));
        this.errorType = cause;
    }

    public ErrorType getErrorType() {
        return errorType;
    }
    
    public enum ErrorType {
        /**
         * Quando um parâmetro obrigatório não é informado
         */
        MISSING_INPUT, 
        /**
         * Quando um parâmetro é inválido
         */
        BAD_INPUT, 
        /**
         * Quando um parâmetro é válido, mas não pode ser aceito para o estado
         * atual do componente.
         */
        BAD_STATE
    }
}
