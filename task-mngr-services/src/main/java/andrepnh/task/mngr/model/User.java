package andrepnh.task.mngr.model;

import static com.google.common.base.MoreObjects.*;
import static com.google.common.base.Preconditions.*;
import static andrepnh.task.mngr.model.InputPreconditions.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public final class User implements Entity<String> {
    // Podemos tirar vantagem de não haver persistência e ter o id da entidade
    // como a identidade de acordo com o domínio da aplicação.
    private final String userName;
    
    private String displayName;
    
    private String hashedPassword;
    
    private final List<Task> tasksTaken = new ArrayList<>();
    
    public User(String userName, String displayName, String password) {
        userName = validateNotNull(userName, "O usuário é obrigatório").trim();
        validateArgument(!userName.isEmpty(), "O usuário não pode ser vazio");
        this.userName = userName;
        setDisplayName(displayName);
        setHashedPassword(password);
    }
    
    public void takeTask(Task pendingTask) {
        checkNotNull(pendingTask).markAsTaken(this);
        tasksTaken.add(pendingTask);
    }
    
    public void dropTask(Task taskTaken) {
        validateArgument(
            this.equals(checkNotNull(taskTaken).getUser().orElse(null)),
            "O usuário %s não pode largar a tarefa %d, porque esta atribuida a %s",
            userName, taskTaken.getCode(), taskTaken.getUser().map(User::getUserName));
        taskTaken.markAsPending();
        tasksTaken.remove(taskTaken);
    }
    
    public void completeTask(Task taskTaken, String answer) {
        validateArgument(
            this.equals(checkNotNull(taskTaken).getUser().orElse(null)),
            "O usuário %s não pode completar a tarefa %d, porque esta atribuida a %s",
            userName, taskTaken.getCode(), taskTaken.getUser().map(User::getUserName));
        taskTaken.markAsCompleted(this, answer);
        tasksTaken.remove(taskTaken);
    }
    
    public String getUserName() {
        return userName;
    }
    
    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        displayName = validateNotNull(displayName, "O nome do usuário é obrigatório").trim();
        validateArgument(!displayName.isEmpty(), "O nome do usuário não pode ser vazio");
        this.displayName = displayName;
    }

    /**
     * @return uma cópia da lista de tarefas atribuídas a esse usuário.
     */
    public List<Task> getTasksTaken() {
        // cópia defensiva
        return new ArrayList<>(tasksTaken);
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(String hashedPassword) {
        hashedPassword = validateNotNull(hashedPassword, "A senha é obrigatória");
        validateArgument(!hashedPassword.isEmpty(), "A senha não pode ser apenas whitespace");
        this.hashedPassword = hashedPassword;
    }
    
    @Override
    public String getId() {
        return userName;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.userName);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.userName, other.userName)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return toStringHelper(User.class)
            .add("userName", userName)
            .add("displayName", displayName)
            .add("tasksTaken", tasksTaken)
            .toString();
    }
}
