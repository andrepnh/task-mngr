package andrepnh.task.mngr.model;

import java.net.URL;
import java.util.Objects;

// Deliberadamente mantido fora da classe User
public class UserAvatar {
    private final User user;
    
    // Poderíamos ter outras fontes de imagens, mas usaremos apenas url para simplificar
    private final URL url;

    public UserAvatar(User user, URL url) {
        this.user = Objects.requireNonNull(user);
        this.url = Objects.requireNonNull(url);
    }

    public User getUser() {
        return user;
    }

    public URL getUrl() {
        return url;
    }
}
