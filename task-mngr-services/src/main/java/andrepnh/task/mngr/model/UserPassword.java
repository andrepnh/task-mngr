package andrepnh.task.mngr.model;

import static com.google.common.base.Preconditions.*;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Representa a senha de usuário. Mantê-la separada da entidade é útil para fins
 * de auditória e permite adicionar outros mecanismos de autenticação posteriormente.
 */
public class UserPassword implements Entity<UUID> {
    private final UUID uuid = UUID.randomUUID();
    
    private final User user;
    
    private final String hash;
    
    private final LocalDateTime createdAt;
    
    private LocalDateTime revokedAt;

    public UserPassword(User user, String hash) {
        String trimmedHash = checkNotNull(hash, "hash nulo").trim();
        checkArgument(!trimmedHash.isEmpty(), 
            "hash vazio ou apenas whitespace: %s",
            hash);
        this.user = checkNotNull(user, "usuário nulo");
        this.hash = trimmedHash;
        createdAt = LocalDateTime.now();
    }
    
    public void revoke() {
        revokedAt = LocalDateTime.now();
    }

    public User getUser() {
        return user;
    }

    public String getHash() {
        return hash;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public LocalDateTime getRevokedAt() {
        return revokedAt;
    }
    
    @Override
    public UUID getId() {
        return uuid;
    }
    
}
