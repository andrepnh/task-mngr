package andrepnh.task.mngr.model;

import andrepnh.task.mngr.model.BadInputException.ErrorType;

/**
 * Lança {@link BadInputException} em caso de falhas ao checar invariantes.
 */
public final class InputPreconditions {
    private InputPreconditions() {
    }
    
    public static <T> T validateNotNull(T input, String message, Object... args) {
        if (input == null) {
            throw new BadInputException(message, ErrorType.MISSING_INPUT, args);
        }
        return input;
    }
    
    public static void validateArgument(boolean condition, String message, Object... args) {
        if (!condition) {
            throw new BadInputException(message, ErrorType.BAD_INPUT, args);
        }
    }
    
    public static void validateState(boolean condition, String message, Object... args) {
        if (!condition) {
            throw new BadInputException(message, ErrorType.BAD_STATE, args);
        }
    }
}
