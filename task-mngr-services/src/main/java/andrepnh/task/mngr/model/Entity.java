package andrepnh.task.mngr.model;

/**
 * @param <I> tipo do id da entidade
 */
public interface Entity<I> {
    I getId();
}
