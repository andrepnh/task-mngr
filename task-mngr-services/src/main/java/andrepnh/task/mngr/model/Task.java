package andrepnh.task.mngr.model;

import static com.google.common.base.MoreObjects.*;
import static com.google.common.base.Preconditions.*;
import static andrepnh.task.mngr.model.InputPreconditions.*;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

public class Task implements Entity<Integer> {
    private static final AtomicInteger SERIAL = new AtomicInteger(1);
    
    private final int code;
    
    private final LocalDateTime creationDate;
    
    private final String description;
    
    private String answer;
    
    private User takenBy;
    
    private User completedBy;

    public Task(String description) {
        String trimmedDescription = validateNotNull(
            description, "A descrição é obrigatória").trim();
        validateArgument(!trimmedDescription.isEmpty(),
            "A descrição de uma tarefa não pode ser vazia ou apenas whitespace, "
                + "obteve: %s",
            description);
        this.code = SERIAL.getAndIncrement();
        this.description = trimmedDescription;
        creationDate = LocalDateTime.now();
    }

    public Status getStatus() {
        if (completedBy != null) {
            return Status.COMPLETED;
        } else if (takenBy != null) {
            return Status.TAKEN;
        } else {
            return Status.PENDING;
        }
    }
    
    void markAsTaken(User takenBy) {
        validateState(getStatus() == Status.PENDING, 
            "A tarefa %d tem status %s e não pode ser pega.",
            code, getStatus());
        // Não fazemos maiores validações do usuário, isso é responsabilidade da classe User
        this.takenBy = checkNotNull(takenBy);
    }
    
    void markAsPending() {
        validateState(getStatus() == Status.PENDING || getStatus() == Status.TAKEN, 
            "A tarefa %d tem status %s não pode ser largada.",
            code, getStatus());
        this.takenBy = null;
    }
    
    void markAsCompleted(User completedBy, String answer) {
        validateState(getStatus() == Status.TAKEN,
            "A tarefa %d tem status %s e não pode ser completada.",
            code, getStatus());
        // Não fazemos maiores validações do usuário, isso é responsabilidade da classe User
        this.completedBy = checkNotNull(completedBy);
        this.takenBy = null;
        this.answer = answer;
    }
    
    public Optional<User> getTakenBy() {
        return Optional.ofNullable(takenBy);
    }

    public Optional<User> getCompletedBy() {
        return Optional.ofNullable(completedBy);
    }
    
    /**
     * @return o usuário que pegou ou o que completou a tarefa
     */
    public Optional<User> getUser() {
        return getTakenBy().isPresent() ? getTakenBy() : getCompletedBy();
    }

    public int getCode() {
        return code;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public String getDescription() {
        return description;
    }

    public Optional<String> getAnswer() {
        return Optional.ofNullable(answer);
    }
    
    @Override
    public Integer getId() {
        return code;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + this.code;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Task other = (Task) obj;
        if (this.code != other.code) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return toStringHelper(Task.class)
            .add("code", code)
            .add("creationDate", creationDate)
            .add("description", description)
            .add("answer", answer)
            .add("takenBy", getTakenBy().map(User::getUserName))
            .add("completedBy", getCompletedBy().map(User::getUserName))
            .add("status", getStatus())
            .toString();
    }

    public enum Status {
        PENDING, TAKEN, COMPLETED
    }
}
