package andrepnh.task.mngr.services;

import andrepnh.task.mngr.model.Task;
import andrepnh.task.mngr.model.User;
import andrepnh.task.mngr.repositories.TaskRepository;
import andrepnh.task.mngr.repositories.UserRepository;
import static com.google.common.base.Preconditions.*;
import com.google.inject.Inject;
import java.util.Optional;

public class UserService {
    private final AuthenticationService authenticationService;
    
    private final UserRepository userRepository;
    
    private final TaskRepository taskRepository;

    @Inject
    public UserService(AuthenticationService authenticationService, 
        UserRepository userRepository, 
        TaskRepository taskRepository) {
        this.authenticationService = checkNotNull(authenticationService);
        this.userRepository = checkNotNull(userRepository);
        this.taskRepository = checkNotNull(taskRepository);
    }
    
    public User createUser(String userName, String displayName, String plainPassword) {
        String hashedPassword = authenticationService.hash(plainPassword);
        User newUser = new User(userName, displayName, hashedPassword);
        return userRepository.insert(newUser);
    }
    
    public Task createTask(User user, String description) {
        Task task = new Task(description);
        takeTask(user, task);
        return task;
    }
    
    public Task completeTask(User user, Task task, String answer) {
        user.completeTask(task, answer);
        userRepository.update(user);
        return taskRepository.update(task);
    }
    
    public Task dropTask(User user, Task task) {
        user.dropTask(task);
        userRepository.update(user);
        return taskRepository.update(task);
    }
    
    public Task takeTask(User user, Task task) {
        user.takeTask(task);
        userRepository.update(user);
        return taskRepository.update(task);
    }

    public Optional<User> findById(String id) {
        return userRepository.findById(id);
    }
    
}
