package andrepnh.task.mngr.services;

public class UnauthorizedAccessException extends RuntimeException {

    private String username;

    public UnauthorizedAccessException() {
    }
    
    public UnauthorizedAccessException(String username) {
        super(String.format("%s; senha ou usuário inválido", username));
        this.username = username;
    }

    public String getUsername() {
        return username;
    }
    
}
