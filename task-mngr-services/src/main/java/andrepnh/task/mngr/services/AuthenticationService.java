package andrepnh.task.mngr.services;

import andrepnh.task.mngr.model.User;
import andrepnh.task.mngr.repositories.UserRepository;
import java.util.Optional;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.inject.Inject;

public class AuthenticationService {
    private final UserRepository userRepository;

    @Inject
    public AuthenticationService(UserRepository userRepository) {
        this.userRepository = checkNotNull(userRepository);
    }
    
    public void authenticate(String userName, String inputPassword) {
        Optional<User> optUser = userRepository.findById(userName);
        User user = optUser.orElseThrow(() -> new UnauthorizedAccessException(userName));
        if (!user.getHashedPassword().equals(hash(inputPassword))) {
            throw new UnauthorizedAccessException(userName);
        }
    }
    
    public String hash(String plainText) {
        return String.valueOf(plainText.hashCode());
    }
}
