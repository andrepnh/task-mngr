package andrepnh.task.mngr.services;

import andrepnh.task.mngr.model.Task;
import andrepnh.task.mngr.repositories.TaskRepository;
import static com.google.common.base.Preconditions.*;
import com.google.inject.Inject;
import java.util.List;
import java.util.Optional;

public class TaskService {
    private final TaskRepository repository;

    @Inject
    public TaskService(TaskRepository repository) {
        this.repository = checkNotNull(repository);
    }
    
    public Optional<Task> findById(Integer id) {
        return repository.findById(id);
    }

    public List<Task> findAll() {
        return repository.findAll();
    }
}
