## Deploy
A aplicação é composta de 3 jars (maiores detalhes na seção "Motivação"):

1. task-mngr-services: model e serviços da aplicação
2. task-mngr-rest: API restful
3. task-mngr-web: frontend

Os 3 jars usam maven, então para compilar os jars basta executar:

```bash
cd task-mngr-services
mvn clean install
cd ../task-mngr-rest
mvn clean install
cd ../task-mngr-web
mvn clean install
```

Com os jars compilados, primeiro subimos o servidor da API:

```bash
cd .. # De volta a raiz do projeto
java -jar task-mngr-rest/target/task-mngr-rest-0.0.1-SNAPSHOT.jar server task-mngr-rest/task-mngr-rest.yml
```
Com o servidor no ar, execute o jar do servidor web em outro terminal:
```bash
java -jar task-mngr-web/target/task-mngr-web-0.0.1-SNAPSHOT.jar server task-mngr-web/task-mngr-web.yml
```

O frontend da aplicação pode ser acessado em http://localhost:8080/index.html

## Tecnologias usadas
Todas as aplicações são Java 8 e usam features como a API de streams, de tempo, Optional, etc.

1. task-mngr-services: guava, guice
2. task-mngr-rest: guava, guice, dropwizard, jersey, jackson, hibernate validator
3. task-mngr-web: dropwizard como servidor de arquivos estáticos e, para o desenvolvimento do frontend, node.js, webpack, react com JSX, ES6, bootstrap

## Limitações
Alguns projetos, basicamente o frontend, são protótipos inacabados e deixam a desejar no quesito organização e features implementadas.

A autenticação foi implementada no servidor, mas o filtro de autenticação do Jersey acabou sendo retirado, porque há um problema não resolvido da função fetch() do javascript não enviar o cabeçaalho Authorization - mesmo com o CORS habilitado para todas as origens.
A ideial inicial era usar autenticação Basic seguida de tokens. A token seria armazenada no client-side no localStorage e o servidor contaria com uma interface para armazenamento de "sessões", permitindo escalar o frontend e a API sem ter que usar session-pinning em load balancers. Sem o request inicial de autenticação funcionando, nada disso foi feito e o formulário de login sã encaminha para a página principal.

A criação de tarefas no frontend, última feature que trabalhei, também está com um problema não determinado com a função fetch() e não está completa. Não houve tempo para implementar a próxima feature de listar detalhes de uma tarefa. Também não foi possível trabalhar a apresentação da aplicação que só está usando estilos do bootstrap não customizados.

## Motivação

O módulo conta com algumas separações lógicas, sendo elas, da mais até a menos externa: o modelo, com as entidades da aplicação e um pouco de lógica; classes de acesso aos dados; serviços que implementam a lógica mais complexa do domínio; uma API restful para servir dados e uma aplicação de frontend voltada para servir html, javascript, css, imagens, etc.

As classes do modelo da aplicação não são anêmicas (https://www.martinfowler.com/bliki/AnemicDomainModel.html) e contam com alguma lógica simples que pertence ao seu domínio. Uma vantagem clara dessa abordagem é que trazendo um pouco do comportamento do sitema para essa camada, temos automaticamente simplicidade nos testes, já que o modelo não trabalha com nada IO bound ou complexo (bancos de dados, sistemas de arquivos, redes, threads, etc).
Entretanto, considerando o tamanho do módulo, não considero que vale a pena ir além disso e implementar DDD completo, incluindo CQRS e Event Sourcing. Não só pela complexidade desnecessária, mas também porque um modelo de Event Sourcing, por exemplo, não é útil para os requisitos da aplicação, uma vez que não há menção a auditoria ou analytics.

A camada de acesso aos dados existe para isolar detalhes de persistência e de busca do restante da aplicação. A implementação atual são estruturas de dados em memória com queries baseadas na API de streams do Java 8, o que pessoalmente acho mais simples e produtivo que um banco SQL em memória como H2 ou Derby.
Essa parte do módulo faz uso intenso de interfaces para permitir trocar entre implementações diferentes de persistência. Isso é útil até para testes, onde seria possível trocar o banco relacional da produção por essas estruturas de dados em memória.

Os serviços incluem o restante da lógica do domínio da aplicação e são, majoritamente, transaction scripts (https://www.martinfowler.com/eaaCatalog/transactionScript.html). Um padrão que pode se tornar um problema caso o sistema cresça, mas faz sentido para um micro-serviço pequeno e simples, tornando o desenvolvimento mais produtivo.
Do ponto de vista dos artefatos do sistema, a camada de serviços inclui a de acesso aos dados e o modelo, todos gerando um único jar. Novamente, acho que esse modelo faz mais sentido para algo simples, uma vez que dificilmente partes superiores do sistema usariam apenas uma dessas camadas isoladas das outras. Isso facilita o deploy e versionamento dos artefatos e, de qualquer forma, dentro do jar de serviços cada camada é separada em pacotes distintos - se necessário podemos movê-los para projetos diferentes.
Com essas camadas separadas num jar, estamos livres para incluir novas formas de integração que dependem dele, como talvez uma API baseada em GraphQL ou um serviço que use mensageria para integrações internas.

O frontend do sistema é separado da API restful. Esse sim é um ponto que acredito valer a pena um pouco mais de complexidade no deploy da aplicação. Servir uma API e servir arquivos majoritariamente estáticos são duas preocupações bem diferentes.
Com o frontend separado, num cenário real poderíamos distribuir os css e javascript otimizados usando uma CDN. Também estaríamos livres para usar o processamento do servidor para server-side components usando node.js ou ainda node.js + nashorn. 
No frontend a sessão do usuário é definida numa interface diretamente sob controle da aplicação. Com isso, num ambiente de produção, poderíamos definir uma implementação que armazena a sessão num banco como o Redis, para que possamos usar mais de uma instância do frontend antecedida de um load balancer - sem necessidade de fazer session pinning.
Ainda existe a vantagem de escalar separadamente. Servir arquivos estáticos permite uma série de otimizações que não são possíveis numa API. Com isso podemos monitorar separadamente a carga sobre cada tipo de servidor web e usar mais instâncias da API conforme necessário. Também poderíamos eventualmente adicionar novos mecanismos de autenticação, talvez usando um API gateway que também permitiria fazer throttling e coibir abusos no uso da integração, sem impacto direto no frontend. 